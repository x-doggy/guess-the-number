const chalk = require("chalk");

function ChalkedLogger() {
  const LOGLEVEL = {
    SUCCESS: val => console.log(chalk.bold.bgGreen(val)),
    ERROR: val => console.error(chalk.bold.bgRed(val)),
    INFO: val => console.log(chalk.inverse(val)),
    TITLE: val => console.log(chalk.bold.yellow.bgBlue(val))
  };
  ChalkedLogger.LOGLEVEL = LOGLEVEL;
  this.log = function (msg, level = LOGLEVEL.SUCCESS) {
    return this.logMultiple([
      Array(msg.length + 5).join(" "),
      "  " + msg + "  ",
      Array(msg.length + 5).join(" ")
    ], level);
  }
  
  this.logMultiple = function (msgs, level = LOGLEVEL.TITLE) {
    return msgs.forEach(level);
  }
}

module.exports = ChalkedLogger;

