const chalk = require("chalk");
const argv = require('minimist')(process.argv.slice(2));
const packageJson = require("../package.json");

const ver = packageJson.version;
const verString = chalk.bgBlack(
  chalk.cyan.inverse(" Guess the number ") + " mini game version " + chalk.red.bgWhite.underline(ver)
);
let lang = "ru";

if (Object.keys(argv).length === 2) {
  if (argv.v || argv.version) {
    // Get version from package.json
    console.log(verString);
    process.exit(0);
  } else if (
    argv.lang && typeof argv.lang === "string" ||
    argv.L    && typeof argv.L    === "string"
  ) {
    // Set a lang
    lang = argv.lang || argv.L;
  }
}

module.exports = { lang, verString };
