const readline = require("readline");
const chalk = require("chalk");
const strings = require("./strings.json");

function GuessTheNumberGame(guess, loggerClass, lang) {
  this.guess = guess;
  this.loggerClass = loggerClass;
  this.logger = new loggerClass();
  this.lang = lang;
  this.success = false;
  this.attempts = 0;
  this.getString = function (key, replaceWord) {
    const gotString = strings[this.lang][key];
    if (typeof replaceWord !== "undefined") {
      return gotString.replace("%s", replaceWord);
    }
    return gotString;
  };  
  this.r = readline.createInterface({
    input  : process.stdin,
    output : process.stdout,
    prompt : chalk.bold(this.getString("enterNumber")),
  });
  this.r.on("line", d => this.processLine(d)).on("close", () => this.processClose());
  this.hello = function () {
    console.clear();
    const title = this.getString("title");
    this.logger.logMultiple(title, this.loggerClass.LOGLEVEL.TITLE);
    return this;
  };
  this.processLine = function (d) {
    const y = +d.trim();
    this.attempts += 1;
    if (Number.isNaN(y) || typeof y === "undefined") {
      this.logger.log(this.getString("incorrectData"), this.loggerClass.LOGLEVEL.ERROR);
      this.r.prompt();
    } else {
      if (y === this.guess) {
        this.success = true;
        this.r.close();
      } else {
        if (y > 1000 || y < 1) {
          this.logger.log(this.getString("outOfRange"), this.loggerClass.LOGLEVEL.ERROR);
        } else {
          this.logger.log(`x ${(this.guess > y ? ">" : "<")} y`, this.loggerClass.LOGLEVEL.INFO);
        }
        this.r.prompt();
      }
    }
  };
  this.processClose = function () {
    if (this.success) {
      this.logger.log(this.getString("success", this.attempts), this.loggerClass.LOGLEVEL.SUCCESS);
    } else {
      console.log("\n");
      this.logger.log(this.getString("failure", this.attempts), this.loggerClass.LOGLEVEL.INFO);
    }
  };
  this.prompt = function () {
    this.r.prompt();
    return this;
  };
}

module.exports = GuessTheNumberGame;

