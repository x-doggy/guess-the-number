# "Guess the number" game

Written by me in [Node.js](https://nodejs.org). To play you need to install dependencies:

```bash
npm i
```

And then start this way:

```bash
node index.js
```

or that way:

```bash
./index.js
```

Also, `--lang` or `-L` CLI options is available to set the language:

```bash
./index.js --lang=en # or
./index.js -L en
```

Finally, `-v` or `--version` options prints version of the game:

```bash
./index.js -v # or
./index.js --version
```

