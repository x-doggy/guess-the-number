#!/usr/bin/env node
/*
 * +-------------------------------------------------------------------+
 * |                    "Guess The Number" game                        |
 * |             Guess the number using computer's tips.               |
 * |                                                                   |
 * |  @author:   Vladimir Stadnik                                      |
 * |  @telegram: tele.click/x_doggy                                    |
 * |  @date:     25.12.2017 00:23:07                                   |
 * +-------------------------------------------------------------------+
 */

const Logger = require("./lib/ChalkedLogger");
const GuessTheNumberGame = require("./lib/GuessTheNumberGame");
const { lang } = require("./lib/ArgvHandler");

new GuessTheNumberGame(Math.floor(Math.random() * 1001), Logger, lang)
  .hello()
  .prompt();
